class CustomTinyCNN(nn.Module):
    def __init__(self):
        super(CustomTinyCNN, self).__init__()
        # First convolution layer
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=2, kernel_size=3, padding=1)
        # First ReLU
        self.relu1 = nn.ReLU()
        # First pooling
        self.pool1 = nn.MaxPool2d(kernel_size=4)
        # Second convolution layer
        self.conv2 = nn.Conv2d(in_channels=2, out_channels=2, kernel_size=3, padding=1)
        # Second ReLU
        self.relu2 = nn.ReLU()
        # Second pooling
        self.pool2 = nn.MaxPool2d(kernel_size=2)
        # First layer of classifier
        self.fc1 = nn.Linear(2 * 4 * 4, 32)
        # Third ReLU
        self.relu3 = nn.ReLU()
        # Second layer of classifier (output layer)
        self.fc2 = nn.Linear(32, 15)
        
    def forward(self, x):
        print("Input shape:", x.shape)
        
        x = self.conv1(x)
        print("After conv1 shape:", x.shape)
        
        x = self.relu1(x)
        print("After relu1 shape:", x.shape)
        
        x = self.pool1(x)
        print("After pool1 shape:", x.shape)
        
        x = self.conv2(x)
        print("After conv2 shape:", x.shape)
        
        x = self.relu2(x)
        print("After relu2 shape:", x.shape)
        
        x = self.pool2(x)
        print("After pool2 shape:", x.shape)
        
        x = x.view(-1, 2 * 4 * 4)
        print("After view shape:", x.shape)
        
        x = self.fc1(x)
        print("After fc1 shape:", x.shape)
        
        x = self.relu3(x)
        print("After relu3 shape:", x.shape)
        
        x = self.fc2(x)
        print("After fc2 shape:", x.shape)
        
        return x

# Instantiate the model
custom_tiny_cnn = CustomTinyCNN()

# Create a DataLoader to handle batching
dataloader = DataLoader(dataset, batch_size=4, shuffle=False)

# Get a single batch from the DataLoader
images, labels = next(iter(dataloader))

print('Images shape', images.shape)
print('labels shape', labels.shape)

# Use the model to predict the batch
output = custom_tiny_cnn(images)

print('Output shape', output.shape)
                        
